﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.BodyBasics
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Forms;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using System.Timers;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        

        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        private int i = 0;
       

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;
        private DrawingGroup drawingGroup2;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;
        private DrawingImage imageSource2;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        //boolean to get wether recording or replaying is activated
        private Boolean recording = false;
        private Boolean replaying = false;

        //init path for replaying
        private string replayingPath= Environment.CurrentDirectory + "\\Default.csv";
        
        //replaying only needs one traking of joints 
        private Boolean firstTraking = true;
        // store the starding time of the recording
        private DateTime recordingStart;
        //stores the starting time of the replaying
        private DateTime replayStart= DateTime.Now;
        private Double[] result;
        //folder for storing the files
        private string directory = Environment.CurrentDirectory + "\\";
        //Stream for writing to a file
        System.IO.StreamWriter file = null;
        // Point thickness for the coordinate system
        private const double PointThickness = 1;
        //max amount of joints
        private const int maxDifJoints = 25;
        // stream for reading a file
        private System.IO.StreamReader reader = null;
        private double time;
        //array of colors for coordinate system 
        private List<SolidColorBrush> color= new List<SolidColorBrush>{ Brushes.Black, Brushes.Green, Brushes.Yellow, Brushes.Red, Brushes.Blue, Brushes.Violet, Brushes.Brown, Brushes.Orange, Brushes.Olive, Brushes.Orchid, Brushes.Salmon, Brushes.YellowGreen, Brushes.SandyBrown, Brushes.RoyalBlue, Brushes.Purple, Brushes.Plum, Brushes.MediumSpringGreen, Brushes.Black, Brushes.Green, Brushes.Yellow, Brushes.Red, Brushes.Blue, Brushes.Violet, Brushes.Brown, Brushes.Orange, Brushes.Olive, Brushes.Orchid, Brushes.Salmon, Brushes.YellowGreen, Brushes.SandyBrown, Brushes.RoyalBlue, Brushes.Purple, Brushes.Plum };
        private FormattedText formattedText = new FormattedText("x", CultureInfo.GetCultureInfo("en-us"), 0, new Typeface("Verdana"),32,   Brushes.Black);
        // joint types storing order 
        private JointType[] jTypes = { JointType.Head, JointType.Neck, JointType.SpineShoulder, JointType.SpineMid, JointType.ShoulderRight, JointType.ShoulderLeft,JointType.SpineBase,JointType.HipRight, JointType.HipLeft, JointType.ElbowRight, JointType.WristRight, JointType.HandRight, JointType.ThumbRight, JointType.HandTipRight, JointType.ElbowLeft, JointType.WristLeft, JointType.HandLeft, JointType.ThumbLeft, JointType.HandTipLeft};
        // storage of points for the coodinate system
        private LinkedList<Point>[] pointStorage = new LinkedList<Point>[maxDifJoints];
      
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // get the depth (display) extents
            FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;


            // get size of joint space
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

            
            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            // Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<Pen>();

            this.bodyColors.Add(new Pen(Brushes.Red, 6));
            this.bodyColors.Add(new Pen(Brushes.Orange, 6));
            this.bodyColors.Add(new Pen(Brushes.Green, 6));
            this.bodyColors.Add(new Pen(Brushes.Blue, 6));
            this.bodyColors.Add(new Pen(Brushes.Indigo, 6));
            this.bodyColors.Add(new Pen(Brushes.Violet, 6));

            color.Add(Brushes.Black);


            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();
            this.drawingGroup2 = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);
            this.imageSource2 = new DrawingImage(this.drawingGroup2);
            using (DrawingContext dc = this.drawingGroup2.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.White, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                Pen pen = new Pen(Brushes.Black, 1);
                
                dc.DrawLine(pen, new Point(10.0,this.displayHeight-40.0) , new Point(this.displayWidth- 10.0, this.displayHeight - 40.0));
                dc.DrawLine(pen, new Point(10.0, 10.0), new Point(10.0, this.displayHeight - 40.0));

                dc.DrawLine(pen, new Point(6.0, 10.0), new Point(14.0, 10.0));
                dc.DrawLine(pen, new Point(this.displayWidth - 10.0, this.displayHeight - 36.0), new Point(this.displayWidth - 10.0, this.displayHeight - 44.0));

                dc.DrawLine(pen, new Point(4.0, this.displayHeight - 40.0), new Point(14.0, this.displayHeight - 40.0));
                dc.DrawLine(pen, new Point(10.0, this.displayHeight - 36.0), new Point(10.0, this.displayHeight - 44.0));
            }



            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// Gets the bitmap to display coodinate system
        public ImageSource ImageSource2
        {
            get
            {
                return this.imageSource2;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
                
            }
           // Console.WriteLine(System.IO.Directory.GetCurrentDirectory() + ".csv");
            
         }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
            if (recording)
            {
                this.file.Close();
            }
           }

        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {

            
                bool dataReceived = false;

                using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
                {
                    if (bodyFrame != null)
                    {
                        if (this.bodies == null)
                        {
                            this.bodies = new Body[bodyFrame.BodyCount];
                        }

                        // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                        // As long as those body objects are not disposed and not set to null in the array,
                        // those body objects will be re-used.
                        bodyFrame.GetAndRefreshBodyData(this.bodies);
                        dataReceived = true;
                    }
                }
            // display current kinect frame if not replaying
            if (!replaying)
            {
                if (dataReceived)
                {

                    using (DrawingContext dc = this.drawingGroup.Open())
                    {
                        // Draw a transparent background to set the render size
                        dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        int penIndex = 0;

                        foreach (Body body in this.bodies)
                        {
                            Pen drawPen = this.bodyColors[penIndex++];

                            if (body.IsTracked)
                            {
                                this.DrawClippedEdges(body, dc);

                                IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                                // convert the joint points to depth (display) space
                                Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                                foreach (JointType jointType in joints.Keys)
                                {
                                    // sometimes the depth(Z) of an inferred joint may show as negative
                                    // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                    CameraSpacePoint position = joints[jointType].Position;
                                    if (position.Z < 0)
                                    {
                                        position.Z = InferredZPositionClamp;
                                    }

                                    DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                    jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                                }

                                this.DrawBody(joints, jointPoints, dc, drawPen);

                                this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                                // Get relevant coordinates from skeleton and write to file
                                if (recording)
                                {
                                    this.getCoordinates(joints, jointPoints);
                                }
                                // Console.WriteLine((long)((DateTime.Now - recordingStart).TotalMilliseconds) + ";");
                            }
                        }



                        // prevent drawing outside of our render area
                        this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                    }
                }
            }
            else
            {
                //if replay mode is activated
                using (DrawingContext drc = this.drawingGroup.Open())
                {

                    drc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                    this.replayBodyMovement(drc);
                }
            }
                       
         }

        // methode for replaying the body movements
        private void replayBodyMovement(DrawingContext drawingContext)
        {
           
            if (reader == null)
            {
                reader = new System.IO.StreamReader(File.OpenRead(replayingPath));
                reader.ReadLine();
                replayStart = DateTime.Now;
                getNextLine(reader.ReadLine());


            }
            else
            {
                if (reader.EndOfStream)
                {
                    if (firstTraking)
                    {
                        drawTracking();
                        firstTraking = false;
                    }
                    reader = new System.IO.StreamReader(File.OpenRead(replayingPath));
                    reader.ReadLine();
                    replayStart = DateTime.Now;
                    getNextLine(reader.ReadLine());

                }
                else
                {
                    //Console.Write("read");
                    double timestamp = (double)(DateTime.Now - replayStart).TotalMilliseconds;
                    if (timestamp >= result[0])
                    {
                        getNextLine(reader.ReadLine());
                    }
                    Pen drawPen = this.bodyColors[4];

                    //init joints
                                       for (int f = 0; f < jTypes.Length; f++)
                    {
                        if (pointStorage[f] == null)
                        {
                            pointStorage[f] = new LinkedList<Point>();
                        }


                    }
                    if (result.Length > 76)
                    {
                        Point headpoint = new Point(result[4], result[5]);
                        Point neckpoint = new Point(result[9], result[10]);
                        Point spineShoulderpoint = new Point(result[14], result[15]);
                        Point spineMidpoint = new Point(result[19], result[20]);
                        Point shoulderRightpoint = new Point(result[24], result[25]);
                        Point shoulderLeftpoint = new Point(result[29], result[30]);
                        Point spineBasepoint = new Point(result[34], result[35]);
                        Point hipRightpoint = new Point(result[39], result[40]);
                        Point hipLeftpoint = new Point(result[44], result[45]);
                        Point ellbowRight = new Point(result[39], result[40]);
                        Point wristRight = new Point(result[44], result[45]);
                        Point handRight = new Point(result[39], result[40]);
                        Point tumphRight = new Point(result[44], result[45]);
                        Point fingerTRight = new Point(result[49], result[50]);
                        Point ellbowLeft = new Point(result[54], result[55]);
                        Point wristLeft = new Point(result[59], result[60]);
                        Point handLeft = new Point(result[64], result[65]);
                        Point tumphLeft = new Point(result[69], result[70]);
                        Point fingerTLeft = new Point(result[74], result[75]);

                        
                        // if the first Traking store all points
                        if (firstTraking)
                        {
                            //store point into point Storage
                            pointStorage[0].AddLast(headpoint);
                            pointStorage[1].AddLast(neckpoint);
                            pointStorage[2].AddLast(spineShoulderpoint);
                            pointStorage[3].AddLast(spineMidpoint);
                            pointStorage[4].AddLast(shoulderRightpoint);
                            pointStorage[5].AddLast(shoulderLeftpoint);
                            pointStorage[6].AddLast(spineBasepoint);
                            pointStorage[7].AddLast(hipRightpoint);
                            pointStorage[8].AddLast(hipLeftpoint);
                            pointStorage[9].AddLast(ellbowRight);
                            pointStorage[10].AddLast(wristRight);
                            pointStorage[11].AddLast(handRight);
                            pointStorage[12].AddLast(tumphRight);
                            pointStorage[13].AddLast(fingerTRight);
                            pointStorage[14].AddLast(ellbowLeft);
                            pointStorage[15].AddLast(wristLeft);
                            pointStorage[16].AddLast(handLeft);
                            pointStorage[17].AddLast(tumphLeft);
                            pointStorage[18].AddLast(fingerTLeft);


                        }

                        //draw joints
                        drawingContext.DrawEllipse(Brushes.Red, null, headpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, neckpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, spineShoulderpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, spineMidpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, shoulderRightpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, shoulderLeftpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, spineBasepoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, hipRightpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, hipLeftpoint, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, ellbowRight, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, wristRight, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, handRight, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, tumphRight, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, fingerTRight, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, ellbowLeft, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, wristLeft, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, handLeft, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, tumphLeft, JointThickness, JointThickness);
                        drawingContext.DrawEllipse(Brushes.Red, null, fingerTLeft, JointThickness, JointThickness);


                        //draw bone
                        drawingContext.DrawLine(drawPen, headpoint, neckpoint);
                        drawingContext.DrawLine(drawPen, neckpoint, spineShoulderpoint);
                        drawingContext.DrawLine(drawPen, spineShoulderpoint, shoulderLeftpoint);
                        drawingContext.DrawLine(drawPen, spineShoulderpoint, shoulderRightpoint);
                        drawingContext.DrawLine(drawPen, spineShoulderpoint, spineMidpoint);
                        drawingContext.DrawLine(drawPen, spineMidpoint, spineBasepoint);
                        drawingContext.DrawLine(drawPen, spineBasepoint, hipLeftpoint);
                        drawingContext.DrawLine(drawPen, spineBasepoint, hipRightpoint);
                        drawingContext.DrawLine(drawPen, shoulderRightpoint, ellbowRight);
                        drawingContext.DrawLine(drawPen, ellbowRight, wristRight);
                        drawingContext.DrawLine(drawPen, wristRight, handRight);
                        drawingContext.DrawLine(drawPen, handRight, fingerTRight);
                        drawingContext.DrawLine(drawPen, handRight, tumphRight);
                        drawingContext.DrawLine(drawPen, shoulderLeftpoint, ellbowLeft);
                        drawingContext.DrawLine(drawPen, ellbowLeft, wristLeft);
                        drawingContext.DrawLine(drawPen, wristLeft, handLeft);
                        drawingContext.DrawLine(drawPen, handLeft, fingerTLeft);
                        drawingContext.DrawLine(drawPen, handLeft, tumphLeft);

                    }
                }
                
            }
        }

        private void getNextLine(string line)
        {
            line.Replace(',', '.');
            String[] arry = line.Split(';');
            result = new Double[arry.Length];
            for (int i = 0; i < arry.Length; i++)
            {
                Double.TryParse(arry[i], out result[i]);
            }
        }

        private void getCoordinates(IReadOnlyDictionary<JointType, Joint> joints, Dictionary<JointType, Point> jointPoints)
        {

            file.Write((long)((DateTime.Now - recordingStart).TotalMilliseconds) + ";");
           
            

            for (int f=0; f < jTypes.Length; f++)
            {
                // Shoulder head
                Joint joint = joints[jTypes[f]];
                Point point = jointPoints[jTypes[f]];
                file.Write(joint.Position.X + ";" + joint.Position.Y + ";" + joint.Position.Z + ";" + point.X + ";" + point.Y + ";");
               // Console.Write(joint.Position.X + ";" + joint.Position.Y + ";" + joint.Position.Z + ";" + point.X + ";" + point.Y + ";");
                if (pointStorage[f] == null)
                {
                    pointStorage[f]= new LinkedList<Point> ();  
                }
                pointStorage[f].AddLast(point);
                //Console.WriteLine("add point");

            }
           

            // New Line
            file.WriteLine("");
            Console.WriteLine("");

        }


        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen)
        {
            // Draw the bones
            foreach (var bone in this.bones)
            {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                 
                {

                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);

        }

        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);
                    break;
            }
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext)
        {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }


        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
           if (RecordButton.Content.Equals("Start Recording")){
                RecordButton.Content = "Stop Recording";
                recordingStart = DateTime.Now;
                recording = true;
               // Console.WriteLine(directory);
                String filepath;
                if (file != null)
                {
                    file.Close();
                    }
                if (File.Exists(Environment.CurrentDirectory + recordName.Text + ".csv"))
                {
                    filepath = directory + recordName.Text + ".csv";
                    File.Delete(filepath);
                    
                   // Console.WriteLine("File "+ filepath +"  deleted");
                }               
                this.file = new System.IO.StreamWriter(directory+ recordName.Text + ".csv", true);
                filepath = directory + recordName.Text + ".csv";
                //Console.WriteLine("New File " + filepath + "  created");
                string w = "Timestamp;" +
                            "Head-X;Head-Y;Head-Z;Head-PX;Head-PY;" +
                            "Neck-X;Neck-Y;Neck-Z;Neck-PX;Neck-PY;" +
                            "SpineShoulder-X;SpineShoulder-Y;SpineShoulder-Z;SpineShoulder-PX;SpineShoulder-PY;" +
                            "SpineMid-X;SpineMid-Y;SpineMid-Z;SpineMid-PX;SpineMid-PY;" +
                            "ShoulderR-X;ShoulderR-Y;ShoulderR-Z;ShoulderR-PX;ShoulderR-PY;" +
                            "ShoulderL-X;ShoulderL-Y;ShoulderL-Z;ShoulderL-PX;ShoulderL-PY;" +
                            "SpineBase-X;SpineBase-Y;SpineBAse-Z;SpineBase-PX;SpineBase-PY;" +
                             "HipR-X;HipR-Y;HipR-Z;HipR-PX;HipR-PY;" +
                             "HipL-X;HipL-Y;HipL-Z;HipL-PX;HipL-PY;"+ 
                             "ElbowR-X;ElbowR-Y;ElbowR-Z;ElbowR-PX;ElbowR-PY;" +
                             "WristR-X;WristR-Y;WristR-Z;WristR-PX;WristR-PY;"+
                             "HandR-X;HandR-Y;HandR-Z;HandR-PX;HandR-PY;" +
                             "ElbowR-X;ElbowR-Y;ElbowR-Z;ElbowR-PX;ElbowR-PY;" +
                             "WristR-X;WristR-Y;WristR-Z;WristR-PX;WristR-PY;"+
                             "HandR-X;HandR-Y;HandR-Z;HandR-PX;HandR-PY;" +
                             "TumbR-X;TumbR-Y;TumbR-Z;TumbR-PX;TumbR-PY;"+
                             "HandTR-X;HandTR-Y;HandTR-Z;HandTR-PX;HandTR-PY;"+
                             "ElbowL-X;ElbowL-Y;ElbowR-Z;ElbowL-PX;ElbowL-PY;" +
                             "WristL-X;WristL-Y;WristL-Z;WristL-PX;WristL-PY;"+
                             "HandL-X;HandL-Y;HandL-Z;HandL-PX;HandL-PY;" +
                             "TumbL-X;TumbL-Y;TumbL-Z;TumbL-PX;TumbL-PY;"+ 
                             "HandTL-X;HandTL-Y;HandTL-Z;HandTL-PX;HandTL-PY;";
                file.WriteLine(w);
                //Console.WriteLine(w);
                pointStorage = new LinkedList<Point>[jTypes.Length];

            } else {
                drawTracking();
                file.Close();
                RecordButton.Content = "Start Recording";
                recording = false;
            }
        }

        private void drawTracking()
        {
            using (DrawingContext dc = this.drawingGroup2.Open())
            {   
                //variables to get koordination boundaries
                double minX=double.PositiveInfinity;
                double maxX=double.NegativeInfinity;
                double minY=double.PositiveInfinity;
                double maxY=double.NegativeInfinity;
                Console.WriteLine("paint");

               
                dc.DrawRectangle(Brushes.White, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                Pen pen = new Pen(Brushes.Black, 1);
               
               // draw basic Kordination system
               dc.DrawLine(pen, new Point(10.0, this.displayHeight - 40.0), new Point(this.displayWidth - 10.0, this.displayHeight - 40.0));
                dc.DrawLine(pen, new Point(10.0, 10.0), new Point(10.0, this.displayHeight - 40.0));

                dc.DrawLine(pen, new Point(6.0, 10.0), new Point(14.0, 10.0));
                dc.DrawLine(pen, new Point(this.displayWidth - 10.0, this.displayHeight - 36.0), new Point(this.displayWidth - 10.0, this.displayHeight - 44.0));
                
                dc.DrawLine(pen, new Point(4.0, this.displayHeight - 40.0), new Point(14.0, this.displayHeight - 40.0));
                dc.DrawLine(pen, new Point(10.0, this.displayHeight - 36.0), new Point(10.0, this.displayHeight - 44.0));

                // get ticked jackboxes
                int[] list= new int[jTypes.Length];
                if (checkH.IsChecked.Value) { list[0]= 1; };
                if (checkN.IsChecked.Value) { list[1] = 1; };
                if (checkSS.IsChecked.Value) { list[2] = 1; };
                if (checkSM.IsChecked.Value) { list[3] = 1; };
                if (checkSR.IsChecked.Value) { list[4] = 1; };
                if (checkSL.IsChecked.Value) { list[5] = 1; };
                if (checkSB.IsChecked.Value) { list[6] = 1; };
                if (checkHR.IsChecked.Value) { list[7] = 1; };
                if (checkHL.IsChecked.Value) { list[8] = 1; };
                if (checkElR.IsChecked.Value) { list[9] = 1; };
                if (checkWrR.IsChecked.Value) { list[10] = 1; };
                if (checkHaR.IsChecked.Value) { list[11] = 1; };
                if (checkThR.IsChecked.Value) { list[12] = 1; };
                if (checkHTR.IsChecked.Value) { list[13] = 1; };
                if (checkElL.IsChecked.Value) { list[14] = 1; };
                if (checkWrL.IsChecked.Value) { list[15] = 1; };
                if (checkHaL.IsChecked.Value) { list[16] = 1; };
                if (checkThL.IsChecked.Value) { list[17] = 1; };
                if (checkHTL.IsChecked.Value) { list[18] = 1; };
                


                // get baundries of the coordination system
                for (int f = 0; f < pointStorage.Length; f++)
                {
                    if (pointStorage[f] != null&&list[f]==1)
                        {
                            foreach (Point p in pointStorage[f])
                            {
                                if (p.X > maxX) maxX = p.X;
                                if (p.Y > maxY) maxY = p.Y;
                                if (p.X < minX) minX = p.X;
                                if (p.Y < minY) minY = p.Y;
                            }
                        }
                }
                // get multiplaying factor for the boundries
                double rangeX = (displayWidth-20)/(maxX - minX);
                double rangeY = (displayHeight-50)/(maxY - minY);

                // filter Ininity results 
                if (!(minX == double.PositiveInfinity || maxX == double.NegativeInfinity || minY == double.PositiveInfinity || maxY == double.NegativeInfinity)){
                    
                    // display values of the coordinate system
                    formattedText = new FormattedText(Convert.ToInt32(maxY).ToString(), CultureInfo.GetCultureInfo("en-us"), 0, new Typeface("Verdana"), 32, Brushes.Black);
                    formattedText.SetFontSize(12);
                    dc.DrawText(formattedText, new Point(-20.0, 0.0));

                    formattedText = new FormattedText(Convert.ToInt32(minY).ToString(), CultureInfo.GetCultureInfo("en-us"), 0, new Typeface("Verdana"), 32, Brushes.Black);
                    formattedText.SetFontSize(12);
                    dc.DrawText(formattedText, new Point(-20.0, this.displayHeight - 50.0));

                    formattedText = new FormattedText(Convert.ToInt32(minX).ToString(), CultureInfo.GetCultureInfo("en-us"), 0, new Typeface("Verdana"), 32, Brushes.Black);
                    formattedText.SetFontSize(12);
                    dc.DrawText(formattedText, new Point(10.0, this.displayHeight - 35.0));

                    formattedText = new FormattedText(Convert.ToInt32(maxX).ToString(), CultureInfo.GetCultureInfo("en-us"), 0, new Typeface("Verdana"), 32, Brushes.Black);
                    formattedText.SetFontSize(12);
                    dc.DrawText(formattedText, new Point(this.displayWidth - 15.0, this.displayHeight - 35.0));
                }

                
                for (int f = 0; f < pointStorage.Length; f++)
                {
                    //filter points which should be mapped
                    if (pointStorage[f] != null&&list[f]==1)
                    {
                       // draw a all points
                        foreach (Point p in pointStorage[f])
                        {
                            dc.DrawEllipse(color[f], null, new Point ((p.X-minX)*rangeX, (p.Y-minY)*rangeY), PointThickness, PointThickness);
                        }
                    }
                }
            }
        }
        private void recordName_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!replaying) {
                replayingPath = replayPath.Text;
             } 
        }

    // Replay funktion 
        private void OnClickReplay(object sender, RoutedEventArgs e)
        {
            // if replay is not started yet           
            if (replay.Content.Equals("start Replay"))
            {
                // get the path of the .csv file
                replayingPath = directory+replayPath.Text;
                replay.Content = "stop Replay";
                firstTraking = true;
                replaying = true;
                // reset the point storage
                pointStorage = new LinkedList<Point>[jTypes.Length];
            }
            else
            {
            
                replay.Content = "start Replay";
                replaying = false;
            }
        }
    }
}